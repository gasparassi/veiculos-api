<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\VehicleController;
use App\Http\Controllers\VehicleTipController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\Auth\AuthController;

/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
 */

Route::get(
    '/',
    function () {
        return response()
            ->json(
                [
                    'message' => 'Veículos API',
                    'status' => 'Connected',
                    'statusCode' => 200,
                ],
                200
            );
    }
);

Route::group([
    'middleware' => 'users',
    'prefix' => 'v1',
], function () {
    Route::group([
        'prefix' => 'users',
    ], function () {
        Route::get('/vehicle-tips/{id}', [UserController::class, 'showVehicleTipsFromUser']);
        Route::post('/login', [AuthController::class, 'login'])->name('users.login')->withoutMiddleware('users');
        Route::post('/register', [UserController::class, 'store'])->name('users.store')->withoutMiddleware('users');
        Route::get('/', [UserController::class, 'index'])->name('users.index');
        Route::get('/{id}', [UserController::class, 'show'])->name('users.show');
        Route::put('/{id}', [UserController::class, 'update'])->name('users.update');
        Route::delete('/{id}', [UserController::class, 'destroy'])->name('usersv.destroy');
    });
    Route::group([
        'prefix' => 'vehicles-tips',
    ], function () {
        Route::post('/', [VehicleTipController::class, 'store'])->name('vehicle-tip.store');
        Route::get('/', [VehicleTipController::class, 'index'])->name('vehicle-tip.index')->withoutMiddleware('users');;
        Route::get('/{id}', [VehicleTipController::class, 'show'])->name('vehicle-tip.show');
        Route::put('/{id}', [VehicleTipController::class, 'update'])->name('vehicle-tip.update');
        Route::delete('/{id}', [VehicleTipController::class, 'destroy'])->name('vehicle-tip.destroy');
    });
    Route::resource('vehicles', VehicleController::class)->except('edit', 'create');
});
