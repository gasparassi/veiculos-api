FROM php:7.3-apache

RUN apt-get update

RUN apt-get install -y \
    git \
    zip \
    wget \
    curl \
    sudo \
    unzip \
    libicu-dev \
    libbz2-dev \
    libpng-dev \
    libjpeg-dev \
    libmcrypt-dev \
    libreadline-dev \
    libfreetype6-dev \
    libzip-dev \
    g++ \
    vim \
    tzdata

# Install XDebug
RUN pecl install xdebug

# Configure XDebug
RUN docker-php-ext-enable xdebug

ENV APACHE_DOCUMENT_ROOT=/var/www/html/public
RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
RUN sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf

RUN a2enmod rewrite headers

RUN mv "$PHP_INI_DIR/php.ini-development" "$PHP_INI_DIR/php.ini"

RUN docker-php-ext-install \
    bz2 \
    intl \
    iconv \
    bcmath \
    opcache \
    calendar \
    mbstring \
    pdo_mysql \
    gd \
    zip

RUN apt install -y libldap2-dev
RUN docker-php-ext-configure ldap --with-libdir=lib/x86_64-linux-gnu
RUN docker-php-ext-install ldap

# Expose port 9000 and start php-fpm server
EXPOSE 9000

COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

ARG uid

RUN echo $uid
RUN useradd -G www-data,root -u $uid devuser -m
RUN mkdir -p /home/devuser/.composer && \
    chown -R devuser:devuser /home/devuser

ENV TZ America/Sao_Paulo
