<?php

namespace App\Repositories;

use App\Repositories\Contracts\VehicleTipRepositotyInterface;
use App\Models\VehicleTip;

/**
 * Description of VehicleTipRepository
 *
 * @author eder
 */
class VehicleTipRepository implements VehicleTipRepositotyInterface
{

    protected $entity;

    public function __construct(VehicleTip $entity)
    {
        $this->entity = $entity;
    }

    public function create(array $resource)
    {
        return $this->entity->create($resource);
    }

    public function delete(object $resource)
    {
        return $resource->delete();
    }

    public function getAll()
    {
        return $this->entity->with('vehicle')->orderBy('created_at')->get();
    }

    public function getOne(int $id)
    {
        return $this->entity->where('id', $id)->first();
    }

    public function update(object $resource, array $resourceArray)
    {
        return $resource->update($resourceArray);
    }
}
