<?php

namespace App\Repositories\Contracts;

/**
 *
 * @author eder
 */
interface VehicleTipRepositotyInterface
{

    public function create(array $resource);

    public function update(object $resource, array $resourceArray);

    public function delete(object $resource);

    public function getOne(int $id);

    public function getAll();
}
