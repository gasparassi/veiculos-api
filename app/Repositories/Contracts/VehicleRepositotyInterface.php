<?php

namespace App\Repositories\Contracts;

use App\Models\Vehicle;

/**
 *
 * @author eder
 */
interface VehicleRepositotyInterface
{

    public function create(array $resource);

    public function update(Vehicle $resource, array $resourceArray): bool;

    public function delete(Vehicle $resource): bool;

    public function getOne(int $id);

    public function getAll();
}
