<?php

namespace App\Repositories;

use App\Repositories\Contracts\VehicleRepositotyInterface;
use App\Models\Vehicle;

/**
 * Description of VehicleRepository
 *
 * @author eder
 */
class VehicleRepository implements VehicleRepositotyInterface
{

    protected $entity;

    public function __construct(Vehicle $entity)
    {
        $this->entity = $entity;
    }

    public function create(array $resource)
    {
        return $this->entity->create($resource);
    }

    public function delete(Vehicle $resource): bool
    {
        return $resource->delete();
    }

    public function getAll()
    {
        return $this->entity->all();
    }

    public function getOne(int $id)
    {
        return $this->entity->where('id', $id)->first();
    }

    public function update(Vehicle $resource, array $resourceArray): bool
    {
        return $resource->update($resourceArray);
    }
}
