<?php

namespace App\Services;

use App\Repositories\Contracts\VehicleTipRepositotyInterface;

/**
 * Description of VehicleTipService
 *
 * @author eder
 */
class VehicleTipService
{

    protected $repository;

    public function __construct(VehicleTipRepositotyInterface $repository)
    {
        $this->repository = $repository;
    }

    public function createNewResource(array $resource)
    {
        return $this->repository->create($resource);
    }

    public function destroyResource(int $id)
    {
        $vehicleTip = $this->repository->getOne($id);

        if ($vehicleTip !== null) {
            $this->repository->delete($vehicleTip);
            return true;
        } else {
            return false;
        }
    }

    public function getAllResources()
    {
        return $this->repository->getAll();
    }

    public function getOneResourceById(int $id)
    {
        return $this->repository->getOne($id);
    }

    public function updateResource(array $resource, int $id)
    {
        $vehicleTip = $this->repository->getOne($id);

        if ($vehicleTip !== null) {
            $this->repository->update($vehicleTip, $resource);
            return $this->repository->getOne($id);
        } else {
            return $vehicleTip;
        }
    }
}
