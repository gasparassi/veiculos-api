<?php

namespace App\Services;

use App\Services\BaseService;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

/**
 * Description of UserService
 *
 * @author eder
 */
class UserService implements BaseService
{

    protected $entity;

    public function __construct(User $entity)
    {
        $this->entity = $entity;
    }

    public function createNewResource(array $resource)
    {
        return $this->entity->create($resource);
    }

    public function destroyResource(int $id)
    {
        $user = $this->getOneResourceById($id);

        if ($user !== null) {
            $user->delete();
            
            return true;
        } else {
            return false;
        }
    }

    public function getAllResources()
    {
        return $this->entity->all();
    }

    public function getOneResourceById(int $id)
    {
        return $this->entity->where('id', $id)->first();
    }

    public function updateResource(array $resource, int $id)
    {
        $user = $this->getOneResourceById($id);

        if ($user !== null) {
            $user->update($resource);

            return $this->getOneResourceById($id);
        } else {
            return $user;
        }
    }

    public function getOneUserByIdWithVehicleTips($id)
    {
        return $this->entity->where('id', $id)->first();
    }
    
    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\Guard
     */
    public function guard()
    {
        return Auth::guard('users');
    }
    
    public function getUserByEmail($email)
    {
        return $this->entity->where('email', $email)
                        ->get(['id', 'name', 'email']);
    }
}
