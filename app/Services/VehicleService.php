<?php

namespace App\Services;

use App\Repositories\VehicleRepository;

/**
 * Description of VehicleService
 *
 * @author eder
 */
class VehicleService
{

    protected $repository;

    public function __construct(VehicleRepository $repository)
    {
        $this->repository = $repository;
    }

    public function createNewVehicle(array $resource)
    {
        return $this->repository->create($resource);
    }

    public function destroyVehicle(int $id)
    {
        $vehicle = $this->repository->getOne($id);

        if ($vehicle !== null) {
            $this->repository->delete($vehicle);
            return true;
        }
        return false;
    }

    public function getAllVehicles()
    {
        return $this->repository->getAll();
    }

    public function getVehicleById(int $id)
    {
        return $this->repository->getOne($id);
    }

    public function updateVehicle(array $resource, int $id)
    {
        $vehicle = $this->repository->getOne($id);

        if ($vehicle !== null) {
            $this->repository->update($vehicle, $resource);
            return $this->repository->getOne($id);
        } else {
            return $vehicle;
        }
    }
}
