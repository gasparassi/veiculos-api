<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    use HasFactory;

    protected $table = 'vehicles';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type',
        'mark',
        'model',
        'version'
    ];

    public function vehicleTips()
    {
        return $this->hasMany(VehicleTip::class, 'vehicle_id', 'id');
    }
}
