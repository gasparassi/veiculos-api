<?php

namespace App\Http\Requests;

use App\Http\Requests\FormRequestCustom as FormRequest;
use Illuminate\Validation\Rule;

class UserUpdateRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:100',
            'password' => 'required|min:6|confirmed',
            'email' => [
                'required',
                Rule::unique('users', 'email')->ignore($this->user),
            ],
        ];
    }
}
