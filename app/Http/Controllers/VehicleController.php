<?php

namespace App\Http\Controllers;

use App\Http\Requests\VehicleRequest;
use App\Services\VehicleService;
use App\Http\Resources\VehicleResource;

class VehicleController extends Controller
{

    protected $service;

    public function __construct(VehicleService $service)
    {
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $vehicles = $this->service->getAllVehicles();
            if ($vehicles !== null) {
                return response()->json([
                    'data' => VehicleResource::collection($vehicles),
                    'statusCode' => 200,
                ], 200);
            } else {
                return response()->json([
                    'message' => 'Nenhum veículo cadastrado.',
                    'statusCode' => 404
                ], 404);
            }
        } catch (\Exception $ex) {
            return response()->json([
                'message' => 'Erro não previsto.',
                'error' => $ex->getMessage(),
                'statusCode' => 500
            ], 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\VehicleRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(VehicleRequest $request)
    {
        try {
            $vehicle = $this->service->createNewVehicle($request->all());
            if ($vehicle !== null) {
                return response()->json([
                    'data' => new VehicleResource($vehicle),
                    'statusCode' => 201,
                ], 201);
            } else {
                return response()->json([
                    'message' => 'Erro ao cadastrar o veículo',
                    'statusCode' => 422
                ], 422);
            }
        } catch (\Exception $ex) {
            return response()->json([
                'message' => 'Erro não previsto.',
                'error' => $ex->getMessage(),
                'statusCode' => 500
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $vehicle = $this->service->getVehicleById($id);
            if ($vehicle !== null) {
                return response()->json([
                    'data' => new VehicleResource($vehicle),
                    'statusCode' => 200,
                ], 200);
            } else {
                return response()->json([
                    'message' => 'Veículo não encontrado.',
                    'statusCode' => 404
                ], 404);
            }
        } catch (\Exception $ex) {
            return response()->json([
                'message' => 'Erro não previsto.',
                'error' => $ex->getMessage(),
                'statusCode' => 500
            ], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\VehicleRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(VehicleRequest $request, $id)
    {
        try {
            $vehicle = $this->service->updateVehicle($request->all(), $id);
            if ($vehicle !== null) {
                return response()->json([
                    'data' => new VehicleResource($vehicle),
                    'statusCode' => 200,
                ], 200);
            } else {
                return response()->json([
                    'message' => 'Veículo não encontrado.',
                    'statusCode' => 404
                ], 404);
            }
        } catch (\Exception $ex) {
            return response()->json([
                'message' => 'Erro não previsto.',
                'error' => $ex->getMessage(),
                'statusCode' => 500
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $data = $this->service->destroyVehicle($id);
            if ($data) {
                return response()->json([
                    'data' => 'Veículo removido com sucesso.',
                    'statusCode' => 200,
                ], 200);
            } else {
                return response()->json([
                    'message' => 'Veículo não encontrado.',
                    'statusCode' => 404
                ], 404);
            }
        } catch (\Exception $ex) {
            return response()->json([
                'message' => 'Erro não previsto.',
                'error' => $ex->getMessage(),
                'statusCode' => 500
            ], 500);
        }
    }
}
