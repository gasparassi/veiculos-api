<?php

namespace App\Http\Controllers;

use App\Services\VehicleTipService;
use App\Http\Requests\VehicleTipRequest;
use App\Http\Resources\VehicleTipResource;

class VehicleTipController extends Controller
{

    protected $service;

    public function __construct(VehicleTipService $service)
    {
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $vehicleTips = $this->service->getAllResources();
            if ($vehicleTips !== null) {
                return response()->json([
                            'data' => VehicleTipResource::collection($vehicleTips),
                            'statusCode' => 200,
                                ], 200);
            } else {
                return response()->json([
                            'message' => 'Nenhuma dica de veículo cadastrada.',
                            'statusCode' => 404
                                ], 404);
            }
        } catch (Exception $ex) {
            return response()->json([
                        'message' => 'Erro não previsto.',
                        'error' => $ex->getMessage(),
                        'statusCode' => 500
                            ], 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\VehicleTipRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(VehicleTipRequest $request)
    {
        try {
            $vehicleTip = $this->service->createNewResource($request->all());
            if ($vehicleTip !== null) {
                return response()->json([
                            'data' => new VehicleTipResource($vehicleTip),
                            'statusCode' => 201,
                                ], 201);
            } else {
                return response()->json([
                            'message' => 'Erro ao cadastrar dica do veículo',
                            'statusCode' => 422
                                ], 422);
            }
        } catch (Exception $ex) {
            return response()->json([
                        'message' => 'Erro não previsto.',
                        'error' => $ex->getMessage(),
                        'statusCode' => 500
                            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $vehicleTip = $this->service->getOneResourceById($id);
            if ($vehicleTip !== null) {
                return response()->json([
                            'data' => new VehicleTipResource($vehicleTip),
                            'statusCode' => 200,
                                ], 200);
            } else {
                return response()->json([
                            'message' => 'Dica de veículo não encontrada.',
                            'statusCode' => 404
                                ], 404);
            }
        } catch (Exception $ex) {
            return response()->json([
                        'message' => 'Erro não previsto.',
                        'error' => $ex->getMessage(),
                        'statusCode' => 500
                            ], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\VehicleTipRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(VehicleTipRequest $request, $id)
    {
        try {
            $vehicleTip = $this->service->updateResource($request->all(), $id);
            if ($vehicleTip !== null) {
                return response()->json([
                            'message' => new VehicleTipResource($vehicleTip),
                            'statusCode' => 200,
                                ], 200);
            } else {
                return response()->json([
                            'message' => 'Dica de veículo não encontrada.',
                            'statusCode' => 404
                                ], 404);
            }
        } catch (Exception $ex) {
            return response()->json([
                        'message' => 'Erro não previsto.',
                        'error' => $ex->getMessage(),
                        'statusCode' => 500
                            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $data = $this->service->destroyResource($id);
            if ($data) {
                return response()->json([
                            'data' => 'Dica de veículo removida com sucesso.',
                            'statusCode' => 200,
                                ], 200);
            } else {
                return response()->json([
                            'message' => 'Dica de veículo não encontrada.',
                            'statusCode' => 404
                                ], 404);
            }
        } catch (Exception $ex) {
            return response()->json([
                        'message' => 'Erro não previsto.',
                        'error' => $ex->getMessage(),
                        'statusCode' => 500
                            ], 500);
        }
    }
}
