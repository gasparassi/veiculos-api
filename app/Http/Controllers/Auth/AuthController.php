<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Services\UserService;
use App\Http\Requests\UserLoginRequest;

class AuthController extends Controller
{

    protected $service;

    public function __construct(UserService $service)
    {
        $this->service = $service;
    }

    /**
     * Get a JWT via given credentials.
     *
     * @param  UserLoginRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(UserLoginRequest $request)
    {
        try {
            $credentials = $request->only('email', 'password');
            $token = $this->service->guard()->attempt($credentials);
            if ($token) {
                $user = $this->service->getUserByEmail($request->email);
                return $this->respondWithToken($user, $token);
            } else {
                return response()->json([
                            'error' => 'Credenciais inválidas.',
                            'statusCode' => 422
                                ], 422);
            }
        } catch (Exception $ex) {
            return response()->json([
                        'message' => 'Erro não previsto.',
                        'error' => $ex->getMessage(),
                        'statusCode' => 500
                            ], 500);
        }
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($user, $token)
    {
        $data = new \DateTime('now');
        $minutos = $this->service->guard()->factory()->getTTL() * 60;
        $interval = new \DateInterval('PT' . $minutos . 'M');

        return response()->json([
                    'user' => [
                        'profile' => $user,
                        'access' => [
                            'token' => $token,
                            'token_type' => 'Bearer ',
                            'expires_in' => $data->add($interval)->format('Y-m-d H:i:s'),
                        ],
                    ],
                    'statusCode' => 200,
                        ], 200);
    }
}
