<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Repositories\Contracts\{
    VehicleTipRepositotyInterface,
    VehicleRepositotyInterface,
};
use App\Repositories\{
    VehicleTipRepository,
    VehicleRepositoty
};

class RepositoryServiceProvider extends ServiceProvider
{

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            VehicleTipRepositotyInterface::class,
            VehicleTipRepository::class,
        );

        $this->app->bind(
            VehicleRepositotyInterface::class,
            VehicleRepositoty::class,
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
