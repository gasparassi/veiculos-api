<?php

namespace Database\Factories;

use App\Models\VehicleTip;
use Illuminate\Database\Eloquent\Factories\Factory;

class VehicleTipFactory extends Factory
{

    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = VehicleTip::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => function () {
                return \App\Models\User::all()->random()->id;
            },
            'vehicle_id' => function () {
                return \App\Models\Vehicle::all()->random()->id;
            }
        ];
    }

}
