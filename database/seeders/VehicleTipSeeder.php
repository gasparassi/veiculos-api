<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class VehicleTipSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\VehicleTip::factory(100)->create();
    }

}
